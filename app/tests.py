from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

class UnitTest(TestCase):
    def testHasAccordionButtonOne(self):
        response = self.client.get('/')
        self.assertContains(response, '<button class="accordion">Education</button>')

    def testHasAccordionButtonTwo(self):
        response = self.client.get('/')
        self.assertContains(response, '<button class="accordion">Organizational Experience</button>')

    def testHasAccordionButtonThree(self):
        response = self.client.get('/')
        self.assertContains(response, '<button class="accordion">Current Activities</button>')

    def testHasAccordionButtonFour(self):
        response = self.client.get('/')
        self.assertContains(response, '<button class="accordion">Interests</button>')

    def testAccordionHasPanel(self):
        response = self.client.get('/')
        self.assertContains(response, '<div class="panel">')

    def testPanelHasFlushList(self):
        response = self.client.get('/')
        self.assertContains(response, '<ul class="list-group">')

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.get("http://127.0.0.1:8000")
        super(FunctionalTest,self).setUp() 

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def testAccordionSlideDown(self):        
        buttonOne = self.browser.find_element_by_id("button-one")
        buttonTwo = self.browser.find_element_by_id("button-two")
        buttonThree = self.browser.find_element_by_id("button-three")
        buttonFour = self.browser.find_element_by_id("button-four")

        buttonOne.click()
        buttonTwo.click()
        buttonThree.click()
        buttonFour.click()

        self.assertIn(self.browser.page_source, "Universitas Indonesia")
        self.assertIn(self.browser.page_source, "Olim")
        self.assertIn(self.browser.page_source, "Katalayar")
        self.assertIn(self.browser.page_source, "Project Management")
